  # Part I

 Examples of Injection Flaws

In part one of the report, examples of injection flaws are included. They are also included in the file "injec-flaws.py"

SQL Injection
The SQL injection example ' OR '1'='1 is a simple yet effective method to bypass authentication mechanisms or manipulate SQL queries for malicious purposes. It takes advantage of poor input validation and unsanitized user input in SQL queries.

To understand how this works, consider a basic login scenario where a user provides a username and password. The application checks the provided credentials against the information stored in a database. The SQL query for this authentication might look like:
SELECT * FROM users WHERE username='<input_username>' AND password='<input_password>';
Now, imagine an attacker enters the following input as the username: admin' OR '1'='1. The SQL query becomes:
SELECT * FROM users WHERE username='admin' OR '1'='1' AND password='<input_password>';
In this case, the OR '1'='1' part of the query will always evaluate to true because 1 equals 1. As a result, the entire query becomes true if there is a user named "admin" in the users table, regardless of whether the password is correct or not. This can lead to unauthorized access if the first user in the users table is an administrator.

Similarly, if the attacker provides ' OR '1'='1 as the input for both username and password fields, the SQL query becomes:
SELECT * FROM users WHERE username='' OR '1'='1' AND password='' OR '1'='1';
In this case, both the username and password conditions evaluate to true because of the OR '1'='1' condition, and the query returns the first user in the users table. This allows the attacker to bypass the authentication mechanism and gain unauthorized access to the system.

To prevent SQL injection attacks like this, developers should use prepared statements, parameterized queries, or stored procedures to separate user input from the actual SQL query, ensuring that user input cannot modify the query's structure. Additionally, input validation and output encoding should be implemented to minimize the risk of SQL injection attacks.

Command Injection
The example used is:  ; rm -rf /
Command injection is a type of vulnerability where an attacker can execute arbitrary system commands by injecting malicious input into an application. In this example, the payload ; rm -rf / is designed to execute a harmful command on Unix-based systems.

The semicolon ; is used to separate multiple commands on a Unix-based system. When the application doesn't properly sanitize user input or validate it, the attacker can use the semicolon to inject additional commands.

The command rm -rf / is a dangerous one:

rm: It stands for "remove" and is used to delete files or directories.
-r: The flag indicates that the removal should be done recursively, deleting all subdirectories and their contents.
-f: The flag forces the removal without asking for confirmation.
/: It represents the root directory of the system.
So, rm -rf / will attempt to delete all files and directories on the system starting from the root directory, without asking for confirmation. This command can cause catastrophic damage if executed with the necessary permissions.

To prevent command injection, developers should avoid using user input directly in system commands, use proper input validation and sanitization, and apply the principle of least privilege when assigning permissions to applications.

LDAP Injection
The example used is:  *)(uid=*))(|(uid=*
LDAP (Lightweight Directory Access Protocol) injection is a type of vulnerability that occurs when an application constructs LDAP queries without properly validating or sanitizing user input. In this example, the payload *)(uid=*))(|(uid=* is designed to manipulate an LDAP query.

Consider an LDAP search filter that looks like this:
(uid=<input_uid>)
The filter searches for a record with a specific uid attribute, as provided by the user. If an attacker inputs the payload *)(uid=*))(|(uid=*, the resulting filter becomes:
(uid=*)(uid=*))(|(uid=*)

This malformed filter has two parts:

(uid=*): This filter matches any record with a non-empty uid attribute.
(|(uid=*): This filter is incomplete and gets ignored by the LDAP server.
The final filter (uid=*) returns all records with non-empty uid attributes, potentially allowing the attacker to enumerate or access sensitive information.

To protect against LDAP injection attacks, developers should use prepared statements, parameterized queries, or stored procedures, and apply proper input validation and sanitization. Additionally, applying the principle of least privilege and limiting access to sensitive information can help mitigate the risks associated with LDAP injection attacks.



